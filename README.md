# Runing the project

To start the project run `npm install` and then:

```
npm start
```

and in a separate session:

```
npm run corsProxy
```

Proxy server is needed to use MetaWeather api (they don't send CORS headers). Please note that this is a development time only solution.

# Tests

To run tests use:

```
npm test
```
