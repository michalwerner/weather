import { DayWeather } from "../types/Forecast";
import { getWeatherToDisplay } from "./getWeatherToDisplay";

const sampleDayWeather: DayWeather = {
  id: 123,
  date: "2021-01-01",
  stateAbbr: "xx",
  minTemp: -10.11111,
  maxTemp: 10.11111,
  windSpeed: 5.125,
};

jest.useFakeTimers("modern");

describe("getWeatherToDisplay", () => {
  test("filters out days that are not tomorrow or later", () => {
    const input = [
      sampleDayWeather,
      { ...sampleDayWeather, date: "2021-01-02" },
      { ...sampleDayWeather, date: "2021-01-03" },
      { ...sampleDayWeather, date: "2021-01-04" },
    ];

    jest.setSystemTime(new Date("2021-01-02"));
    const output1 = [
      { ...sampleDayWeather, date: "2021-01-03" },
      { ...sampleDayWeather, date: "2021-01-04" },
    ];
    expect(getWeatherToDisplay(input)).toEqual(output1);

    jest.setSystemTime(new Date("2021-01-06"));
    const output2: DayWeather[] = [];
    expect(getWeatherToDisplay(input)).toEqual(output2);
  });

  test("cuts the number of items to provided value", () => {
    jest.setSystemTime(new Date("2020-12-31"));

    const input = Array(10).fill(sampleDayWeather);

    expect(getWeatherToDisplay(input)).toHaveLength(3);
    expect(getWeatherToDisplay(input, 2)).toHaveLength(2);
    expect(getWeatherToDisplay(input, 0)).toHaveLength(0);
    expect(getWeatherToDisplay(input, 100)).toHaveLength(10);
  });
});
