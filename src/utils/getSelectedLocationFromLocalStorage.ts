import { ForecastLocation } from "../types/ForecastLocation";

export const getSelectedLocationFromLocalStorage = (): ForecastLocation | null => {
  try {
    // This should be validated.
    // Currently user can break application putting corrupted data into localStorage.
    return localStorage.selectedLocation
      ? (JSON.parse(localStorage.selectedLocation) as ForecastLocation)
      : null;
  } catch (e) {
    console.error(
      `Cannot parse saved location from localStorage: ${localStorage.selectedLocation.toString()}`
    );
    return null;
  }
};
