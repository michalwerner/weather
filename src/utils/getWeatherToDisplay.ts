import { endOfToday, isAfter } from "date-fns";

import { DayWeather } from "../types/Forecast";

export const getWeatherToDisplay = (
  weather: DayWeather[],
  itemsToDisplay: number = 3
): DayWeather[] => {
  const today = endOfToday();

  return weather
    .filter((i) => isAfter(new Date(i.date), today))
    .slice(0, itemsToDisplay);
};
