import { TemperatureUnit } from "../types/TemperatureUnit";

export const toFahrenheit = (value: number): number => (value * 9) / 5 + 32;

export const formatTemperature = (
  value: number,
  unit: TemperatureUnit
): string => {
  const convertedValue = unit === "F" ? toFahrenheit(value) : value;
  return `${convertedValue.toFixed(1)} °${unit}`;
};
