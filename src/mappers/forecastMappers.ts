import {
  ApiDayWeather,
  ApiForecast,
  DayWeather,
  Forecast,
} from "../types/Forecast";

export const mapDayWeatherFromApi = (apiValue: ApiDayWeather): DayWeather => ({
  id: apiValue.id,
  date: apiValue.applicable_date,
  stateAbbr: apiValue.weather_state_abbr,
  minTemp: apiValue.min_temp,
  maxTemp: apiValue.max_temp,
  windSpeed: apiValue.wind_speed,
});

export const mapForecastFromApi = (apiValue: ApiForecast): Forecast => ({
  woeid: apiValue.woeid,
  title: apiValue.title,
  weather: apiValue.consolidated_weather.map(mapDayWeatherFromApi),
});
