import {
  ApiForecastLocation,
  ForecastLocation,
} from "../types/ForecastLocation";
import { mapForecastLocationFromApi } from "./locationMappers";

describe("location mappers", () => {
  test("mapForecastLocationFromApi", () => {
    const input: ApiForecastLocation = {
      title: "foo",
      woeid: 123,
    };
    const output: ForecastLocation = { title: "foo", woeid: 123 };
    expect(mapForecastLocationFromApi(input)).toEqual(output);
  });
});
