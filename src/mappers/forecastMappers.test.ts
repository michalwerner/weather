import {
  ApiDayWeather,
  ApiForecast,
  DayWeather,
  Forecast,
} from "../types/Forecast";
import { mapDayWeatherFromApi, mapForecastFromApi } from "./forecastMappers";

const sampleApiDayWeather: ApiDayWeather = {
  id: 123,
  applicable_date: "2021-01-01",
  weather_state_abbr: "xx",
  min_temp: -10.11111,
  max_temp: 10.11111,
  wind_speed: 5.125,
};

const sampleDayWeather: DayWeather = {
  id: 123,
  date: "2021-01-01",
  stateAbbr: "xx",
  minTemp: -10.11111,
  maxTemp: 10.11111,
  windSpeed: 5.125,
};

describe("forecast mappers", () => {
  test("mapDayWeatherFromApi", () => {
    expect(mapDayWeatherFromApi(sampleApiDayWeather)).toEqual(sampleDayWeather);
  });

  test("mapForecastFromApi", () => {
    const input: ApiForecast = {
      woeid: 123,
      title: "foo",
      consolidated_weather: [
        sampleApiDayWeather,
        { ...sampleApiDayWeather, id: 234, applicable_date: "2021-01-02" },
      ],
    };
    const output: Forecast = {
      woeid: 123,
      title: "foo",
      weather: [
        sampleDayWeather,
        { ...sampleDayWeather, id: 234, date: "2021-01-02" },
      ],
    };
    expect(mapForecastFromApi(input)).toEqual(output);
  });
});
