import {
  ApiForecastLocation,
  ForecastLocation,
} from "../types/ForecastLocation";

export const mapForecastLocationFromApi = (
  apiValue: ApiForecastLocation
): ForecastLocation => ({
  title: apiValue.title,
  woeid: apiValue.woeid,
});
