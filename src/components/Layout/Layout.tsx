import { Heading, majorScale, Pane } from "evergreen-ui";
import React from "react";

export const Layout: React.FC<{}> = ({ children }) => (
  <Pane padding={majorScale(4)}>
    <Heading size={700} marginBottom={majorScale(2)}>
      Weather
    </Heading>
    {children}
  </Pane>
);
