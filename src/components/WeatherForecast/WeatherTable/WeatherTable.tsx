import { Table } from "evergreen-ui";
import React from "react";

import { DayWeather } from "../../../types/Forecast";
import { TemperatureUnit } from "../../../types/TemperatureUnit";
import { WeatherTableRow } from "../WeatherTableRow/WeatherTableRow";
import styles from "./WeatherTable.module.scss";

type Props = {
  weatherToDisplay: DayWeather[];
  unit: TemperatureUnit;
};

export const WeatherTable: React.FC<Props> = ({ weatherToDisplay, unit }) => (
  <div className={styles.root}>
    <Table className={styles.table}>
      <Table.Head>
        <Table.TextHeaderCell className={styles.narrowCol} />
        <Table.TextHeaderCell className={styles.narrowCol} />
        <Table.TextHeaderCell>Temp.</Table.TextHeaderCell>
        <Table.TextHeaderCell>Wind [mph]</Table.TextHeaderCell>
      </Table.Head>
      <Table.Body>
        {weatherToDisplay.map((dayWeather) => (
          <WeatherTableRow
            key={dayWeather.id}
            dayWeather={dayWeather}
            unit={unit}
          />
        ))}
      </Table.Body>
    </Table>
  </div>
);
