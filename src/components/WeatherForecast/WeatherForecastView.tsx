import { Alert, Heading, Pane } from "evergreen-ui";
import React from "react";

import { DayWeather } from "../../types/Forecast";
import { TemperatureUnit } from "../../types/TemperatureUnit";
import { TemperatureUnitSwitch } from "./TemperatureUnitSwitch/TemperatureUnitSwitch";
import styles from "./WeatherForecastView.module.scss";
import { WeatherTable } from "./WeatherTable/WeatherTable";

type Props = {
  locationTitle?: string;
  weatherToDisplay: DayWeather[];
  unit: TemperatureUnit;
  setUnit: (value: TemperatureUnit) => void;
};

export const WeatherForecastView: React.FC<Props> = ({
  locationTitle,
  weatherToDisplay,
  unit,
  setUnit,
}) => (
  <Pane>
    <Heading size={500} className={styles.header}>
      Weather forecast for {locationTitle || "?"}
    </Heading>

    {weatherToDisplay.length ? (
      <>
        <TemperatureUnitSwitch unit={unit} setUnit={setUnit} />
        <WeatherTable weatherToDisplay={weatherToDisplay} unit={unit} />
      </>
    ) : (
      <Alert
        className={styles.noData}
        intent="none"
        title="There's no data to display."
      />
    )}
  </Pane>
);
