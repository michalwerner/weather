import React, { useState } from "react";

import { useAppSelector } from "../../store/hooks";
import { TemperatureUnit } from "../../types/TemperatureUnit";
import { getWeatherToDisplay } from "../../utils/getWeatherToDisplay";
import { SpinnerWithMessage } from "../SpinnerWithMessage/SpinnerWithMessage";
import { WeatherForecastView } from "./WeatherForecastView";

export const WeatherForecast: React.FC<{}> = () => {
  const { isFetchingForecast, forecastData } = useAppSelector(
    (state) => state.forecast
  );

  const [unit, setUnit] = useState<TemperatureUnit>("C");

  return (
    <>
      {isFetchingForecast && (
        <SpinnerWithMessage message="Getting your forecast..." />
      )}
      {forecastData && (
        <WeatherForecastView
          locationTitle={forecastData.title}
          weatherToDisplay={getWeatherToDisplay(forecastData.weather)}
          unit={unit}
          setUnit={setUnit}
        />
      )}
    </>
  );
};
