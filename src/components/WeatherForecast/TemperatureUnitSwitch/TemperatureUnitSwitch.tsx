import { SegmentedControl } from "evergreen-ui";
import React from "react";

import { TemperatureUnit } from "../../../types/TemperatureUnit";
import styles from "./TemperatureUnitSwitch.module.scss";

type Props = {
  unit: TemperatureUnit;
  setUnit: (value: TemperatureUnit) => void;
};

export const TemperatureUnitSwitch: React.FC<Props> = ({ unit, setUnit }) => (
  <SegmentedControl
    className={styles.root}
    options={[
      { label: "Celsius", value: "C" },
      { label: "Fahrenheit", value: "F" },
    ]}
    value={unit}
    onChange={(value) => setUnit(value as TemperatureUnit)}
  />
);
