import { format } from "date-fns";
import { Strong, Table } from "evergreen-ui";
import React from "react";

import { DayWeather } from "../../../types/Forecast";
import { TemperatureUnit } from "../../../types/TemperatureUnit";
import { formatTemperature } from "../../../utils/formatTemperature";
import tableStyles from "../WeatherTable/WeatherTable.module.scss";
import styles from "./WeatherTableRow.module.scss";

const getIconUrl = (stateAbbr: string): string =>
  `https://www.metaweather.com/static/img/weather/${stateAbbr}.svg`;

type Props = {
  dayWeather: DayWeather;
  unit: TemperatureUnit;
};

export const WeatherTableRow: React.FC<Props> = ({
  dayWeather: { date, minTemp, maxTemp, windSpeed, stateAbbr },
  unit,
}) => {
  const formattedDate = format(new Date(date), "E");

  const formattedMinTemp = minTemp ? formatTemperature(minTemp, unit) : "?";

  const formattedMaxTemp = maxTemp ? formatTemperature(maxTemp, unit) : "?";

  const formattedWindSpeed = windSpeed?.toFixed(1) ?? "?";

  return (
    <Table.Row className={styles.row}>
      <Table.TextCell className={tableStyles.narrowCol}>
        {formattedDate}
      </Table.TextCell>
      <Table.TextCell className={tableStyles.narrowCol}>
        {stateAbbr ? (
          <img
            className={styles.stateIcon}
            src={getIconUrl(stateAbbr)}
            alt=""
          />
        ) : (
          "?"
        )}
      </Table.TextCell>
      <Table.TextCell>
        <div>
          Min: <Strong>{formattedMinTemp}</Strong>
        </div>
        <div>
          Max: <Strong>{formattedMaxTemp}</Strong>
        </div>
      </Table.TextCell>
      <Table.TextCell>
        <Strong>{formattedWindSpeed}</Strong>
      </Table.TextCell>
    </Table.Row>
  );
};
