import { majorScale, Pane, Spinner, Text } from "evergreen-ui";
import React from "react";

import styles from "./SpinnerWithMessage.module.scss";

type Props = {
  message: string;
};

export const SpinnerWithMessage: React.FC<Props> = ({ message }) => (
  <Pane className={styles.root}>
    <Spinner size={majorScale(3)} className={styles.spinner} />
    <Text>{message}</Text>
  </Pane>
);
