import React from "react";
import { Provider } from "react-redux";

import { store } from "../../store/store";
import { Layout } from "../Layout/Layout";
import { LocationSearch } from "../LocationSearch/LocationSearch";
import { WeatherForecast } from "../WeatherForecast/WeatherForecast";

export const App: React.FC<{}> = () => (
  <Provider store={store}>
    <Layout>
      <LocationSearch />
      <WeatherForecast />
    </Layout>
  </Provider>
);
