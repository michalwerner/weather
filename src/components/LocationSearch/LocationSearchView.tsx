import cx from "classnames";
import {
  Autocomplete,
  GeolocationIcon,
  IconButton,
  majorScale,
  Pane,
  SmallCrossIcon,
  Spinner,
  TextInput,
} from "evergreen-ui";
import React, { ChangeEvent } from "react";

import { ForecastLocation } from "../../types/ForecastLocation";
import { SpinnerWithMessage } from "../SpinnerWithMessage/SpinnerWithMessage";
import styles from "./LocationSearchView.module.scss";

type Props = {
  handleAutocompleteChange: (item: ForecastLocation | null) => void;
  options: ForecastLocation[];
  selectedOption: ForecastLocation | null;
  handleInputChange: (e: ChangeEvent<HTMLInputElement>) => void;
  isFetchingLocations: boolean;
  isUsingGeolocation: boolean;
  handleClear: () => void;
  handleUseGeolocation: () => void;
};

export const LocationSearchView: React.FC<Props> = ({
  handleAutocompleteChange,
  options,
  selectedOption,
  handleInputChange,
  isFetchingLocations,
  isUsingGeolocation,
  handleClear,
  handleUseGeolocation,
}) => (
  <Pane className={styles.root}>
    <div className={styles.autocompleteWrapper}>
      <Autocomplete
        onChange={handleAutocompleteChange}
        items={options}
        itemToString={(item) => item?.title ?? ""}
        selectedItem={selectedOption}
      >
        {({ getInputProps, inputValue, getRef }) => (
          <div className={styles.inputWrapper}>
            <TextInput
              placeholder="Select a city"
              ref={getRef}
              {...getInputProps({
                onChange: handleInputChange,
              })}
              value={inputValue}
              className={cx(styles.input, {
                [styles.loading]: isFetchingLocations,
              })}
            />
            {isFetchingLocations && (
              <Spinner size={majorScale(3)} className={styles.inputSpinner} />
            )}
          </div>
        )}
      </Autocomplete>
      <IconButton icon={SmallCrossIcon} onClick={handleClear} />
      {navigator.geolocation && (
        <IconButton
          icon={GeolocationIcon}
          onClick={handleUseGeolocation}
          disabled={isUsingGeolocation}
        />
      )}
    </div>

    {isUsingGeolocation && (
      <SpinnerWithMessage message="Trying to automatically determine your location..." />
    )}
  </Pane>
);
