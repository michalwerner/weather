import React, { ChangeEvent, useCallback, useEffect, useState } from "react";
import { useDebounce } from "use-debounce";

import { forecastActions } from "../../store/forecastSlice";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { locationActions } from "../../store/locationSlice";
import { ForecastLocation } from "../../types/ForecastLocation";
import { getSelectedLocationFromLocalStorage } from "../../utils/getSelectedLocationFromLocalStorage";
import { LocationSearchView } from "./LocationSearchView";

export const LocationSearch: React.FC<{}> = () => {
  const dispatch = useAppDispatch();

  const {
    options,
    selectedOption,
    isFetchingLocations,
    isUsingGeolocation,
  } = useAppSelector((state) => state.location);

  const [inputValue, setInputValue] = useState<string>("");
  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value);
  };
  const [debouncedInputValue] = useDebounce(inputValue, 500);

  useEffect(() => {
    if (debouncedInputValue) {
      dispatch(locationActions.fetchLocations(debouncedInputValue));
    }
  }, [dispatch, debouncedInputValue]);

  const handleSelect = useCallback(
    (item: ForecastLocation | null) => {
      dispatch(locationActions.select(item));
      item && dispatch(forecastActions.fetchForecast(item.woeid));
      localStorage.selectedLocation = JSON.stringify(item);
    },
    [dispatch]
  );

  const handleClear = () => {
    dispatch(locationActions.select(null));
    dispatch(forecastActions.clear());
    localStorage.selectedLocation = null;
  };

  useEffect(() => {
    const savedLocation = getSelectedLocationFromLocalStorage();
    if (savedLocation) {
      handleSelect(savedLocation);
    } else if (navigator.geolocation) {
      dispatch(locationActions.geolocate());
    }
  }, [dispatch, handleSelect]);

  const handleUseGeolocation = () => {
    handleClear();
    dispatch(locationActions.geolocate());
  };

  return (
    <LocationSearchView
      handleAutocompleteChange={handleSelect}
      options={options}
      selectedOption={selectedOption}
      handleInputChange={handleInputChange}
      isFetchingLocations={isFetchingLocations}
      isUsingGeolocation={isUsingGeolocation}
      handleClear={handleClear}
      handleUseGeolocation={handleUseGeolocation}
    />
  );
};
