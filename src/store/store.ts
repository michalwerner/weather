import { configureStore } from "@reduxjs/toolkit";

import { forecastReducer } from "./forecastSlice";
import { locationReducer } from "./locationSlice";

export const store = configureStore({
  reducer: {
    location: locationReducer,
    forecast: forecastReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
