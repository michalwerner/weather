import { toaster } from "evergreen-ui";

import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { mapDayWeatherFromApi } from "../mappers/forecastMappers";
import { ApiForecast, Forecast } from "../types/Forecast";

type ForecastState = {
  forecastData: Forecast | null;
  isFetchingForecast: boolean;
};

const initialState: ForecastState = {
  forecastData: null,
  isFetchingForecast: false,
};

const mapForecastFromApi = (apiValue: ApiForecast): Forecast => ({
  woeid: apiValue.woeid,
  title: apiValue.title,
  weather: apiValue.consolidated_weather.map(mapDayWeatherFromApi),
});

const SLICE_PREFIX = "forecast";

const fetchForecast = createAsyncThunk(
  `${SLICE_PREFIX}/fetchForecast`,
  async (woeid: number) => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_METAWEATHER_API_ROOT}/location/${woeid}`
      );
      if (response.status !== 200) {
        throw new Error(`${response.status} ${response.statusText}`);
      }
      return (await response.json()) as ApiForecast;
    } catch (e) {
      toaster.danger("Sorry, something went wrong.");
      console.error(e);
      throw e;
    }
  }
);

export const forecastSlice = createSlice({
  name: SLICE_PREFIX,
  initialState,
  reducers: {
    clear: (state) => {
      state.forecastData = null;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchForecast.pending, (state) => {
      state.forecastData = null;
      state.isFetchingForecast = true;
    });
    builder.addCase(fetchForecast.fulfilled, (state, action) => {
      state.forecastData = mapForecastFromApi(action.payload);
      state.isFetchingForecast = false;
    });
    builder.addCase(fetchForecast.rejected, (state) => {
      state.isFetchingForecast = false;
    });
  },
});

export const forecastActions = {
  ...forecastSlice.actions,
  fetchForecast,
};

export const forecastReducer = forecastSlice.reducer;
