import { toaster } from "evergreen-ui";

import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";

import { mapForecastLocationFromApi } from "../mappers/locationMappers";
import {
  ApiForecastLocation,
  ForecastLocation,
} from "../types/ForecastLocation";
import { getGeolocationPosition } from "../utils/getGeolocationPosition";
import { forecastActions } from "./forecastSlice";

type LocationState = {
  options: ForecastLocation[];
  selectedOption: ForecastLocation | null;
  isFetchingLocations: boolean;
  isUsingGeolocation: boolean;
};

const initialState: LocationState = {
  options: [],
  selectedOption: null,
  isFetchingLocations: false,
  isUsingGeolocation: false,
};

const SLICE_PREFIX = "location";

const geolocate = createAsyncThunk(
  `${SLICE_PREFIX}/geolocate`,
  async (_arg, { dispatch }): Promise<void> => {
    try {
      const position = await getGeolocationPosition();
      const { latitude: latt, longitude: long } = position.coords;

      const response = await fetch(
        `${process.env.REACT_APP_METAWEATHER_API_ROOT}/location/search/?lattlong=${latt},${long}`
      );
      if (response.status !== 200) {
        throw new Error(`${response.status} ${response.statusText}`);
      }

      const apiNearestLocation = ((await response.json()) as ApiForecastLocation[])?.[0];
      if (apiNearestLocation) {
        const nearestLocation = mapForecastLocationFromApi(apiNearestLocation);
        dispatch(locationActions.select(nearestLocation));
        dispatch(forecastActions.fetchForecast(nearestLocation.woeid));
        localStorage.selectedLocation = JSON.stringify(nearestLocation);
      }

      toaster.success(`Geolocation successful!`);
    } catch (e) {
      if (e instanceof GeolocationPositionError) {
        toaster.danger(`Geolocation failed: ${e.message}.`);
      } else {
        toaster.danger("Sorry, something went wrong.");
      }
      console.error(e);
      throw e;
    }
  }
);

const fetchLocations = createAsyncThunk(
  `${SLICE_PREFIX}/fetchLocations`,
  async (search: string) => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_METAWEATHER_API_ROOT}/location/search/?query=${search}`
      );
      if (response.status !== 200) {
        throw new Error(`${response.status} ${response.statusText}`);
      }
      return (await response.json()) as ApiForecastLocation[];
    } catch (e) {
      toaster.danger("Sorry, something went wrong.");
      console.error(e);
      throw e;
    }
  }
);

export const locationSlice = createSlice({
  name: SLICE_PREFIX,
  initialState,
  reducers: {
    select: (state, action: PayloadAction<ForecastLocation | null>) => {
      state.selectedOption = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(geolocate.pending, (state) => {
      state.isUsingGeolocation = true;
    });
    builder.addCase(geolocate.fulfilled, (state) => {
      state.isUsingGeolocation = false;
    });
    builder.addCase(geolocate.rejected, (state) => {
      state.isUsingGeolocation = false;
    });
    builder.addCase(fetchLocations.pending, (state, action) => {
      state.isFetchingLocations = !!action.meta.arg;
    });
    builder.addCase(fetchLocations.fulfilled, (state, action) => {
      state.options = action.payload.map(mapForecastLocationFromApi);
      state.isFetchingLocations = false;
    });
    builder.addCase(fetchLocations.rejected, (state) => {
      state.isFetchingLocations = false;
    });
  },
});

export const locationActions = {
  ...locationSlice.actions,
  geolocate,
  fetchLocations,
};

export const locationReducer = locationSlice.reducer;
