export type ApiDayWeather = {
  id: number;
  applicable_date: string;
  weather_state_abbr?: string;
  min_temp?: number;
  max_temp?: number;
  wind_speed?: number;
};

export type DayWeather = {
  id: number;
  date: string;
  stateAbbr?: string;
  minTemp?: number;
  maxTemp?: number;
  windSpeed?: number;
};

export type ApiForecast = {
  woeid: number;
  title?: string;
  consolidated_weather: ApiDayWeather[];
};

export type Forecast = {
  woeid: number;
  title?: string;
  weather: DayWeather[];
};
