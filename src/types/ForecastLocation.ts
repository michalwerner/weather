export type ApiForecastLocation = {
  title: string;
  woeid: number;
};

export type ForecastLocation = {
  title: string;
  woeid: number;
};
